# Guía gratis de iniciación a desarrollar aplicaciones móviles en Android

Guía de iniciación sencilla y rápida con más de 30 páginas para aprender a desarrollar aplicaciones para Android. En esta guía aprenderás a usar el SDK de Android y lograrás aprender lo siguiente:


- Entender los componentes básicos de las aplicaciones Android
- Conocerás e instalarás el entorno de trabajo Android Studio y el SDK de Android.
- Conocer la estructura de las aplicaciones Android.
- Conocer el sistema de construcción de proyectos Gradle.
- Aprenderás cómo funciona la librería Retrofit para consumir APIs REST.
- Aprenderás y pondrás en práctica lo aprendido con una aplicación sencilla en Android para -comprobar el tiempo meteorológico en un código postal.

![Captura de la aplicación](https://lh3.googleusercontent.com/gh14rAPXK4WCJ7wVCxsl1R4tANdLfEUtCEZ-oXP_w_i_5ihct5vUx47cIsWFmeUB5N4pxbRdvFB4vdc=w1920-h956-rw)


### consigue la guía [aquí](https://dargo.net)


###### Guía escrita por [Costular](http://costular.es) para [Dargo](https://dargo.net)