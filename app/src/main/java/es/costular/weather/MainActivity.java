package es.costular.weather;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import es.costular.weather.weather.WeatherFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new WeatherFragment())
                .commit();
    }
}
