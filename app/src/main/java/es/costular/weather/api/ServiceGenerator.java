package es.costular.weather.api;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by costular on 09/01/17.
 */

public class ServiceGenerator {

    public static final String API_BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/";
    // Obtenemos la URL base de la API a través de su documentación

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());
    // Creamos el objeto Retrofit con el conversor a Gson.

    public static Retrofit retrofit() {
        return builder.build();
    }

    /*
     * Creamos una clase estática que nos permitirá instanciar pasándole el service por argumento
     */
    public static <S> S createService(Class<S> serviceClass, final String appId) {
        httpClient.addInterceptor(new Interceptor() {
            // Añadimos un parámetro a la url en cada petición
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                HttpUrl url = originalHttpUrl.newBuilder()
                        .addQueryParameter("appid", appId) // ?appid=api-key en cada petición
                        .addQueryParameter("lang", "es")
                        .addQueryParameter("units", "metric")
                        .build();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .url(url);

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        httpClient.addInterceptor(logging);
        OkHttpClient client = httpClient.build();
        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }
}

