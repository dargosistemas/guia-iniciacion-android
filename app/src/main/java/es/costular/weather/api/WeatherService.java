package es.costular.weather.api;

import es.costular.weather.weather.WeatherResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by costular on 09/01/17.
 */

public interface WeatherService {

    @GET("weather")
    Call<WeatherResponse> obtenerTiempoCiudad(@Query("q") String ciudad);
}
