package es.costular.weather.weather;

import com.google.gson.annotations.SerializedName;

/**
 * Created by costular on 09/01/17.
 */

public class MainWeather {

    @SerializedName("temp")
    float temperature;

    int humidity;

    float pressure;

    @SerializedName("temp_min")
    double minTemperature;

    @SerializedName("temp_max")
    double maxTemperature;

}
