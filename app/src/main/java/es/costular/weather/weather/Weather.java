package es.costular.weather.weather;

/**
 * Created by costular on 09/01/17.
 */

public class Weather {

    String country;

    String main;

    String description;

    String icon;

    public String getCountry() {
        return country;
    }

    public String getMain() {
        return main;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }
}
