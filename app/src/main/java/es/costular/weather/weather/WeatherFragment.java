package es.costular.weather.weather;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import es.costular.weather.R;
import es.costular.weather.api.ServiceGenerator;
import es.costular.weather.api.WeatherService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by costular on 09/01/17.
 */

public class WeatherFragment extends Fragment {

    Button buscarCiudadBoton;
    EditText buscarCiudadInput;
    ImageView tiempoIcono;
    TextView tiempoEstado;
    TextView tiempoGrados;
    TextView tiempoCiudad;
    ProgressBar cargando;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_weather, parent, false);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        buscarCiudadBoton = (Button) getView().findViewById(R.id.buscar);
        buscarCiudadInput = (EditText) getView().findViewById(R.id.buscar_ciudad);
        tiempoIcono = (ImageView) getView().findViewById(R.id.weather_icon);
        tiempoEstado = (TextView) getView().findViewById(R.id.weather_estado);
        tiempoGrados = (TextView) getView().findViewById(R.id.weather_temperatura);
        tiempoCiudad = (TextView) getView().findViewById(R.id.weather_ciudad);
        cargando = (ProgressBar) getView().findViewById(R.id.cargando);

        buscarCiudadBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ciudad = buscarCiudadInput.getText().toString();
                buscarCiudad(ciudad);
            }
        });
    }

    private void buscarCiudad(String ciudad) {
        mostrarCargando(true);

        ServiceGenerator.createService(WeatherService.class, getString(R.string.api_key))
                .obtenerTiempoCiudad(ciudad)
                .enqueue(new Callback<WeatherResponse>() {
                    @Override
                    public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                        mostrarCargando(false);

                        if(response.isSuccessful()) {
                            parsearTiempo(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<WeatherResponse> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private void parsearTiempo(WeatherResponse weather) {
        Log.d("WeatherFragment", weather.toString());

        WeatherItem weatherFinal = weather.weathers.get(0);

        tiempoCiudad.setText(weather.city.name);
        tiempoGrados.setText(Float.valueOf(weatherFinal.main.temperature).intValue() + "º");
        tiempoEstado.setText(weatherFinal.weather.get(0).main);

        Log.d("WeatherFragment", "Icono: " + "http://openweathermap.org/img/w/" + weatherFinal.weather.get(0).icon + ".png");

        // Load image
        Picasso.with(getActivity())
                .load("http://openweathermap.org/img/w/" + weatherFinal.weather.get(0).icon + ".png")
                .fit()
                .centerCrop()
                .into(tiempoIcono);

    }

    private void mostrarCargando(boolean mostrar) {
        cargando.setVisibility(mostrar ? View.VISIBLE : View.GONE);
    }
}
