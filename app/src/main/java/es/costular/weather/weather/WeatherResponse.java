package es.costular.weather.weather;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by costular on 09/01/17.
 */

public class WeatherResponse {

    City city;

    @SerializedName("list")
    List<WeatherItem> weathers;

    MainWeather main;

    WindWeather wind;

    String name;
}
